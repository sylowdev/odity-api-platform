# odity-api-platform



## Getting started

composer install 

Then, create the database and its schema:

bin/console doctrine:database:create

bin/console doctrine:migrations:migrate

php -S 127.0.0.1:8000 -t public

http://127.0.0.1:8000/api/graphql

cd config

mkdir jwt

## Generate Keys JWT

cd config/jwt

openssl rsa -in private.pem -pubout -out public.pem

## Create Token

php bin/console doctrine:fixture:load --no-interaction

email :test@test.com
password : test
