<?php


namespace App\DataFixtures;


use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{

  public function load(ObjectManager $manager)
  {
      $user = new User();
      $user->setEmail("test@test.com");
      $user->setPassword('$2y$13$ri1QamPn4x9T3NnxButH4uVJtIx3ky3AT1qCzN1rbyTNerhfwBloq');
      $user->setRoles(['ROLE_USER']);
      $manager->persist($user);
      $manager->flush();
  }
}