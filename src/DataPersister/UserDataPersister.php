<?php


namespace App\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


final class UserDataPersister implements ContextAwareDataPersisterInterface
{
    private $decorated;
    private $passwordEncoder;

    public function __construct(ContextAwareDataPersisterInterface $decorated,UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->decorated = $decorated;
        $this->passwordEncoder = $passwordEncoder;

    }

    public function supports($data, array $context = []): bool
    {
        return $this->decorated->supports($data, $context);
    }

    public function persist($data, array $context = [])
    {

        if($data instanceof User){
            $data->setPassword(
                $this->passwordEncoder->encodePassword($data,$data->getPlainPassword())
            );
            $data->setRoles(['ROLE_USER']);
        }
        $result = $this->decorated->persist($data, $context);
        return $result;
    }

    public function remove($data, array $context = [])
    {
        return $this->decorated->remove($data, $context);
    }




}